﻿using Xceed.Wpf.Toolkit;
using System;
using System.Collections.Generic;
//using System.Windows.Forms;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data.SqlClient;

namespace GraficaCorsi

{
    /// <summary>
    /// Logica di interazione per MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void SelezionaCorso_Load(object sender, EventArgs e)
        {
            /*Dictionary<string,string> comboSource = new Dictionary<string,string>();
            comboSource.Add("1", "Pilates");
            comboSource.Add("2", "Pancafit Group");
            comboSource.Add("3", "Pilates Terza Età");
            comboSource.Add("4", "All Funzionale");
            comboSource.Add("5", "Pilates in Inglese");
            
            SelezionaCorso.DataSource = new BindingSource(comboSource, null);
            SelezionaCorso.DisplayMember = "Value";
            SelezionaCorso.ValueMember = "Key";*/
            try
            {
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
                builder.DataSource = "<server>.database.windows.net";
                builder.UserID = "<username>";
                builder.Password = "<password>";
                builder.InitialCatalog = "<PalestraDB>";
                using (SqlConnection PalestraDB = new SqlConnection(builder.ConnectionString))
                {
                    using (SqlCommand command = new SqlCommand("", PalestraDB))
                    {
                        PalestraDB.Open();
                        List<Corso> corsi = PalestraDB.getCorsi();
                        foreach (Corso corso in corsi)
                        {
                            SelezionaCorso.Items.Add(corso);
                        }
                    }
                }
            }catch (Exception) { }
        }
        private void InserisciCorso_Click(object sender, EventArgs e)
        {
            string key = ((Corso)SelezionaCorso.SelectedItem).ID;
            string value = ((Corso)SelezionaCorso.SelectedItem).nomeCorso;
        }
    }

    public class Corso{
        public string ID;
        public string nomeCorso;

        public string GetID() {
            return ID;
        }

        public string GetnomeCorso() {
            return nomeCorso;
        }
    }
}
